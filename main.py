
import pygame
import bobgui.gui as bobgui

import entities as ents

import random

global in_game
in_game = False

global player

def start_game():
	global in_game
	in_game = True
	global player
	player.lives = 5
	player.score = 0

	player.pos.x = 800/2

def main():
	DISP = pygame.display.set_mode((800,600))

	bobgui.init()
	bobgui.CLR_BASE_TX = (255,0,0)

	global in_game
	global player

	## Gui Stuff

	gui = bobgui.GuiRoot((800,600))

	score_label = bobgui.TextWidget((0,0), "", gui)
	life_label = bobgui.TextWidget((0,16), "", gui)

	start_game_window = bobgui.WindowWidget((800/2+(-300/2), 600/2+(-200/2)), (300,200), gui)
	start_game_button = bobgui.ButtonWidget((50,24), (200,24), None)

	start_game_button.onclick = start_game

	bobgui.TextWidget((0,0), "Start Game", start_game_button)
	start_game_window.add_content(start_game_button)

	player = ents.player((800/2, 500))

	clock = pygame.time.Clock()

	renderlist = [player]

	next_apple = 0

	run = True
	while run:
		frametime = pygame.time.get_ticks()
		
		pygame.display.flip()
		DISP.fill((0,0,0))

		start_game_window.enabled = not in_game

		score_label.set_text("Score: {}".format(player.score))
		life_label.set_text("Lives: {}".format(player.lives))

		for ent in renderlist:
			if ent.die:
				renderlist.remove(ent)
				continue
			ent.update(renderlist, in_game)
			ent.draw()
			ent.render(DISP)

		gui.update()
		gui.render(DISP)

		for event in pygame.event.get():
			
			if event.type == pygame.QUIT:
				run = False
				break
			else:
				gui.send(event)

		
		if in_game and frametime > next_apple:
			next_apple = frametime + random.randint(500,1000)
			print("Apple me")
			renderlist.append(
				ents.FallingApple(
					random.randint( # Spawn the apples close to the player
						round(0+(player.pos.x/2)),
						round(800-(player.pos.x/2))
					)
				)
			)

		if player.lives <= 0:
			in_game = False

		clock.tick(60)

if __name__ == "__main__":
	main()