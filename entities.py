import pygame

Vec2 = pygame.Vector2


class BaseEnt(object):
	
	def __init__(self, pos: Vec2, dims: Vec2):
		self.pos: Vec2 = Vec2(pos)
		self.vel: Vec2 = Vec2(0,0)
		self.dims: Vec2 = Vec2(dims)

		self.die: bool = False # Tells the mainloop to kill us

		self.visual: pygame.Surface = pygame.Surface(dims)

		self.collide: pygame.Rect = pygame.Rect((0,0), dims)

	def draw(self):
		self.visual.fill((255,255,255))

	def render(self, surf: pygame.Surface):
		surf.blit(self.visual, self.pos)

	def send(self, event: pygame.event.Event):
		pass

	def update(self, entlist, in_game):
		self.collide.topleft = self.pos

class player(BaseEnt):

	def __init__(self, pos: Vec2):
		super().__init__(pos, (32,64))

		self.move_dir: int = 0
		self.accl: float = 7.6

		self.score: int = 0
		self.lives: int = 5

	def update(self, entlist, in_game):
		super().update(entlist, in_game)
		held_keys = pygame.key.get_pressed()

		if in_game:
			if held_keys[pygame.K_RIGHT]:
				self.move_dir = 1
			elif held_keys[pygame.K_LEFT]:
				self.move_dir = -1
			else:
				self.move_dir = 0

		self.pos += Vec2(self.move_dir * self.accl, 0)

		if self.pos.x < 0:
			self.pos.x = 0
		elif self.pos.x > (800-self.dims.x):
			self.pos.x = (800-self.dims.x)


		for ent in entlist:
			if ent.collide.colliderect(self.collide) and not ent == self:
				print("collided with ", ent)
				if type(ent) == FallingApple:
					ent.die = True
					self.score += 250

class FallingApple(BaseEnt):

	def __init__(self, xpos: int):
		super().__init__((xpos, -32), (16,16))

	def update(self, entlist, in_game):
		super().update(entlist, in_game)

		self.pos.y += 5

		if self.pos.y > 600:
			self.die = True
			entlist[0].lives -= 1 # HACK! First in the renderlist is always the player